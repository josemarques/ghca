#ifndef SFCR_H 
#define SFCR_H
#include <vector>

//#include "iostream"

std::vector <double> MTU(std::vector<std::vector<double>>ndim);//Multidimensional to unidimensional conversion

int MiTUi(std::vector<int> id, std::vector<int> size);//Returns 1d id for a 2d id
std::vector<int> UiTMi(int id, std::vector<int> size);//Returns multiple d id for a 1d id
#endif
