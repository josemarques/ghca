#include "SFCR.h"
#include "iostream"
int main(){
	std::cout<<"SFCR-TESTS"<<std::endl;
//id conversion test
	std::vector<int> ids={3,2,1};
	std::vector<int> sizes={10,10,10};
	

	std::cout<<"ids=";
	for(int i=0;i<ids.size();i++){
		std::cout<<ids[i]<<",";
	}
	std::cout<<std::endl;

	std::cout<<"sizes=";
	for(int i=0;i<sizes.size();i++){
		std::cout<<sizes[i]<<",";
	}
	std::cout<<std::endl;


	std::cout<<"MiTUi(ids, sizes)="<<MiTUi(ids, sizes)<<std::endl;

	int id=MiTUi(ids, sizes);

	std::vector<int> recid=UiTMi(id, sizes);

	std::cout<<"UiTMi(id, sizes)=";
	for(int i=0;i<recid.size();i++){
		std::cout<<recid[i]<<",";
	}
	std::cout<<std::endl;


return 0;
}
