#!/usr/bin/env python
#Space filing curve routines. Convert N dimensional array to 1 dimension.

#https://stackoverflow.com/questions/17531796/find-the-dimensions-of-a-multidimensional-python-array
def DIM(a):#Returns the number of dimensions of a nested lits. Only for uniforms lists
    if not type(a) == list:
        return []
     return [len(a)] + dim(a[0])
 
 #Traversing the polyhedron vertices of an n-dimensional hypercube in Gray code order produces a generator for the n-dimensional Hilbert curve. "https://mathworld.wolfram.com/HilbertCurve.html"

def BSSTP(nd):#Base step sequence for nd=number of dimensions
	assert nd>1, "For nd>1 only"
	for i in range(nd):
		print(i)

def RTSS(ss):#Rotate step sequence
	for i in range(len(ss)):
		print(i)
	
def TEST(argv):
#Device testing routine

#https://github.com/PrincetonLIPS/numpy-hilbert-curve


if __name__ == '__main__':
	TEST(sys.argv[1:])
