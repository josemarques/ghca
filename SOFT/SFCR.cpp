#include "SFCR.h"

std::vector <double> MTU(std::vector<std::vector<double>>ndim){//Multidimensional to unidimensional conversion
	std::vector<double> unid;
	for(int i=0;i<ndim.size();i++){
		for(int ii=0;ii<ndim[i].size();ii++){
			unid.push_back(ndim[i][ii]);
		}
	}
	return unid;
}

int MiTUi (std::vector<int> id, std::vector<int>size){
int Uid=0;
//Uid=id[0]+id[1]*size[0]+id[2]*(size[0]*size[1]);

for(int i=0;i<id.size();i++){
	int mult=1;
	for(int ii=0;ii<i;ii++){
		mult*=size[ii];
	}
	Uid+=id[i]*mult;
}

return Uid;
}

std::vector<int> UiTMi(int id, std::vector<int> size){//Returns multiple d id for a 1d id
	std::vector<int> idr;
	int cid=id;
	for(int i=0;i<size.size();i++){
		idr.push_back(cid%size[i]);
		//std::cout<<"UiTMi::idr[i]="<<idr[i]<<std::endl;
		cid-=idr[i];
		cid/=size[i];
		//std::cout<<"UiTMi::cid="<<cid<<std::endl;
	}
	return idr;

	// for(int i=size.size()-1;i>0;i--){
	// 	long int curmult=1;
	// 	for(int ii=0;ii<i;ii++){
	// 			curmult*=size[ii];
 //
	// }
	// }

}
